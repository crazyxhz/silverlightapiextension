﻿using ESRI.ArcGIS.Client.Tasks;
using System;

namespace Test
{
	public class AreaAndPerimeterResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the result.
		public MensurationAreaResult Result { get; set; }
	}
	public class CentroidResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the result.
		public MensurationPointResult Result { get; set; }
	}
	public class DistanceAndAngleResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the result.
		public MensurationLengthResult Result { get; set; }
	}
	public class HeightFromBaseAndTopResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the result.
		public MensurationHeightResult Result { get; set; }
	}
	public class HeightFromBaseAndTopShadowResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the result.
		public MensurationHeightResult Result { get; set; }
	}
	public class HeightFromTopAndTopShadowResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the result.
		public MensurationHeightResult Result { get; set; }
	}
	public class PointResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the result.
		public MensurationPointResult Result { get; set; }
	}
}
