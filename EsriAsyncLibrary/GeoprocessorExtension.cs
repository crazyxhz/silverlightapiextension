﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class GeoprocessorExtension
	{
		public static Task<ExecuteResult> ExecuteAsyncTask(this Geoprocessor gp, List<GPParameter> parameters)
		{
			var tcs = new TaskCompletionSource<ExecuteResult>();
			var result = new ExecuteResult();
			EventHandler<GPExecuteCompleteEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
				{
					result.Succeed = true;
					result.Results = e.Results;
					result.UserState = e.UserState;
					tcs.SetResult(result);
					gp.ExecuteCompleted -= handler;
				};
			fail += (s, e) =>
				{
					result.Succeed = false;
					result.Error = e.Error;
					result.UserState = e.UserState;
					tcs.SetResult(result);
					gp.Failed -= fail;
				};
			gp.ExecuteCompleted += handler;
			gp.Failed += fail;
			gp.ExecuteAsync(parameters);
			return tcs.Task;
		}

		public static Task<ExecuteResult> ExecuteAsyncTask(this Geoprocessor gp, List<GPParameter> parameters, object userToken)
		{
			var tcs = new TaskCompletionSource<ExecuteResult>();
			var result = new ExecuteResult();
			EventHandler<GPExecuteCompleteEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.ExecuteCompleted += handler;
			gp.Failed += fail;
			gp.ExecuteAsync(parameters, userToken);
			return tcs.Task;
		}

		public static Task<GetInputResult> GetInputAsyncTask(this Geoprocessor gp, string jobId, string parameterName)
		{
			var tcs = new TaskCompletionSource<GetInputResult>();
			var result = new GetInputResult();
			EventHandler<GPParameterEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Parameter = e.Parameter;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetInputCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetInputCompleted += handler;
			gp.Failed += fail;
			gp.GetInputAsync(jobId, parameterName);
			return tcs.Task;
		}
		public static Task<GetInputResult> GetInputAsyncTask(this Geoprocessor gp, string jobId, string parameterName, object userToken)
		{
			var tcs = new TaskCompletionSource<GetInputResult>();
			var result = new GetInputResult();
			EventHandler<GPParameterEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Parameter = e.Parameter;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetInputCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetInputCompleted += handler;
			gp.Failed += fail;
			gp.GetInputAsync(jobId, parameterName, userToken);
			return tcs.Task;
		}

		public static Task<GetResultDataResult> GetResultDataAsyncTask(this Geoprocessor gp, string jobId, string parameterName)
		{
			var tcs = new TaskCompletionSource<GetResultDataResult>();
			var result = new GetResultDataResult();
			EventHandler<GPParameterEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Parameter = e.Parameter;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetResultDataCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetResultDataCompleted += handler;
			gp.Failed += fail;
			gp.GetResultDataAsync(jobId, parameterName);
			return tcs.Task;
		}

		public static Task<GetResultDataResult> GetResultDataAsyncTask(this Geoprocessor gp, string jobId, string parameterName, object userToken)
		{
			var tcs = new TaskCompletionSource<GetResultDataResult>();
			var result = new GetResultDataResult();
			EventHandler<GPParameterEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Parameter = e.Parameter;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetResultDataCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetResultDataCompleted += handler;
			gp.Failed += fail;
			gp.GetResultDataAsync(jobId, parameterName, userToken);
			return tcs.Task;
		}

		public static Task<GetResultImageResult> GetResultImageAsyncTask(this Geoprocessor gp, string jobId, string parameterName)
		{
			var tcs = new TaskCompletionSource<GetResultImageResult>();
			var result = new GetResultImageResult();
			EventHandler<GetResultImageEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.MapImage = e.MapImage;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetResultImageCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetResultImageCompleted += handler;
			gp.Failed += fail;
			gp.GetResultImageAsync(jobId, parameterName);
			return tcs.Task;
		}
		public static Task<GetResultImageResult> GetResultImageAsyncTask(this Geoprocessor gp, string jobId, string parameterName, object userToken)
		{
			var tcs = new TaskCompletionSource<GetResultImageResult>();
			var result = new GetResultImageResult();
			EventHandler<GetResultImageEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.MapImage = e.MapImage;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetResultImageCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetResultImageCompleted += handler;
			gp.Failed += fail;
			gp.GetResultImageAsync(jobId, parameterName, userToken);
			return tcs.Task;
		}
		public static Task<GetResultImageLayerResult> GetResultImageLayerAsyncTask(this Geoprocessor gp, string jobId, string parameterName)
		{
			var tcs = new TaskCompletionSource<GetResultImageLayerResult>();
			var result = new GetResultImageLayerResult();
			EventHandler<GetResultImageLayerEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.GPResultImageLayer = e.GPResultImageLayer;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetResultImageLayerCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetResultImageLayerCompleted += handler;
			gp.Failed += fail;
			gp.GetResultImageLayerAsync(jobId, parameterName);
			return tcs.Task;
		}
		public static Task<GetResultImageLayerResult> GetResultImageLayerAsyncTask(this Geoprocessor gp, string jobId, string parameterName, object userToken)
		{
			var tcs = new TaskCompletionSource<GetResultImageLayerResult>();
			var result = new GetResultImageLayerResult();
			EventHandler<GetResultImageLayerEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.GPResultImageLayer = e.GPResultImageLayer;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetResultImageLayerCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetResultImageLayerCompleted += handler;
			gp.Failed += fail;
			gp.GetResultImageLayerAsync(jobId, parameterName, userToken);
			return tcs.Task;
		}
		public static Task<GetServiceInfoResult> GetServiceInfoAsyncTask(this Geoprocessor gp)
		{
			var tcs = new TaskCompletionSource<GetServiceInfoResult>();
			var result = new GetServiceInfoResult();
			EventHandler<GPServiceInfoEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.GPServiceInfo = e.GPServiceInfo;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetServiceInfoCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetServiceInfoCompleted += handler;
			gp.Failed += fail;
			gp.GetServiceInfoAsync();
			return tcs.Task;
		}
		public static Task<GetServiceInfoResult> GetServiceInfoAsyncTask(this Geoprocessor gp, object userToken)
		{
			var tcs = new TaskCompletionSource<GetServiceInfoResult>();
			var result = new GetServiceInfoResult();
			EventHandler<GPServiceInfoEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.GPServiceInfo = e.GPServiceInfo;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.GetServiceInfoCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.GetServiceInfoCompleted += handler;
			gp.Failed += fail;
			gp.GetServiceInfoAsync(userToken);
			return tcs.Task;
		}
		public static Task<SubmitJobResult> SubmitJobAsyncTask(this Geoprocessor gp, List<GPParameter> parameters)
		{
			var tcs = new TaskCompletionSource<SubmitJobResult>();
			var result = new SubmitJobResult();
			EventHandler<JobInfoEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.JobInfo = e.JobInfo;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.JobCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.JobCompleted += handler;
			gp.Failed += fail;
			gp.SubmitJobAsync(parameters);
			return tcs.Task;
		}
		public static Task<SubmitJobResult> SubmitJobAsyncTask(this Geoprocessor gp, List<GPParameter> parameters, object userToken)
		{
			var tcs = new TaskCompletionSource<SubmitJobResult>();
			var result = new SubmitJobResult();
			EventHandler<JobInfoEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.JobInfo = e.JobInfo;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.JobCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gp.Failed -= fail;
			};
			gp.JobCompleted += handler;
			gp.Failed += fail;
			gp.SubmitJobAsync(parameters, userToken);
			return tcs.Task;
		}
	}
}
