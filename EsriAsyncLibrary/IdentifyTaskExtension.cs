﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class IdentifyTaskExtension
	{
		public static Task<IdentifyResultExt> ExecuteAsyncTask(this IdentifyTask it, IdentifyParameters identifyParameters)
		{
			var tcs = new TaskCompletionSource<IdentifyResultExt>();
			var result = new IdentifyResultExt();
			EventHandler<IdentifyEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.ExceededTransferLimit = e.ExceededTransferLimit;
				result.IdentifyResults = e.IdentifyResults;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				it.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				it.Failed -= fail;
			};
			it.ExecuteCompleted += handler;
			it.Failed += fail;
			it.ExecuteAsync(identifyParameters);
			return tcs.Task;
		}

		public static Task<IdentifyResultExt> ExecuteAsyncTask(this IdentifyTask it, IdentifyParameters identifyParameters,object userToken)
		{
			var tcs = new TaskCompletionSource<IdentifyResultExt>();
			var result = new IdentifyResultExt();
			EventHandler<IdentifyEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.ExceededTransferLimit = e.ExceededTransferLimit;
				result.IdentifyResults = e.IdentifyResults;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				it.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				it.Failed -= fail;
			};
			it.ExecuteCompleted += handler;
			it.Failed += fail;
			it.ExecuteAsync(identifyParameters, userToken);
			return tcs.Task;
		}
	}
}
