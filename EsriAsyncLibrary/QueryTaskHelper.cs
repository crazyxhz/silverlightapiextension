﻿using ESRI.ArcGIS.Client.Tasks;
using System;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class QueryResult
	{
		/// <summary>
		/// Indicate whether QueryTask succeed
		/// </summary>
		public bool Succeed { get; set; }
		/// <summary>
		/// Copied from QueryEventArgs, indicating whether the number of records returned has exceeded transfer limit (max record count)  
		/// </summary>
		public bool ExceededTransferLimit { get; set; }
		/// <summary>
		/// Copied from QueryEventArgs, The FeatureSet returned by the QueryTask.  
		/// </summary>
		public FeatureSet FeatureSet { get; set; }
		/// <summary>
		/// Copied from TaskFailedEventArgs, the unique identifier for the asynchronous task. (Inherited from ESRI.ArcGIS.Client.Tasks.TaskEventArgs)
		/// </summary>
		public object UserState { get; set; }
		/// <summary>
		/// Copied from TaskFailedEventArgs, The error returned from executing the task.  
		/// </summary>
		public Exception Error { get; set; }
	}

	public class QueryCountResult
	{
		
		/// <summary>
		/// Indicate whether QueryTask succeed
		/// </summary>
		public bool Succeed { get; set; }
		// Summary:
		//     Get the number of features the query would return.
		public int Count { get; set; }
		// Summary:
		//     The error returned from executing the task.
		public Exception Error { get; set; }
		// Summary:
		//     Gets the unique identifier for the asynchronous task.
		public object UserState { get; set; }
	}

	public class RelationshipQueryResult
	{
		public bool Succeed { get; set; }
		public RelationshipResult Result { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
	}
}
