﻿using ESRI.ArcGIS.Client.Geometry;
using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class ComputeHistogramsTaskExtension
	{
		public static Task<ComputeHistogramsResult> ComputeHistogramsAsyncTask(this ComputeHistogramsTask cht, Envelope envelope, ComputeHistogramsParameter parameter, object userToken = null)
		{
			var tcs = new TaskCompletionSource<ComputeHistogramsResult>();
			var result = new ComputeHistogramsResult();
			EventHandler<ComputeHistogramsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				cht.ComputeHistogramsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				cht.Failed -= fail;
			};
			cht.ComputeHistogramsCompleted += handler;
			cht.Failed += fail;
			cht.ComputeHistogramsAsync(envelope, parameter, userToken);
			return tcs.Task;
		}
		public static Task<ComputeHistogramsResult> ComputeHistogramsAsyncTask(this ComputeHistogramsTask cht, Polygon polygon, ComputeHistogramsParameter parameter, object userToken = null)
		{
			var tcs = new TaskCompletionSource<ComputeHistogramsResult>();
			var result = new ComputeHistogramsResult();
			EventHandler<ComputeHistogramsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				cht.ComputeHistogramsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				cht.Failed -= fail;
			};
			cht.ComputeHistogramsCompleted += handler;
			cht.Failed += fail;
			cht.ComputeHistogramsAsync(polygon, parameter, userToken);
			return tcs.Task;
		}


	}
}
