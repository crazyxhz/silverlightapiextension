﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class RouteTaskExtension
	{
		public static Task<SolveResult> SolveAsyncTask(this RouteTask rt, RouteParameters parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<SolveResult>();
			var result = new SolveResult();
			EventHandler<RouteEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.UserState = e.UserState;
				result.Barriers = e.Barriers;
				result.Facilities = e.Facilities;
				result.Incidents = e.Incidents;
				result.Messages = e.Messages;
				result.PolygonBarriers = e.PolygonBarriers;
				result.PolylineBarriers = e.PolylineBarriers;
				result.RouteResults = e.RouteResults;
				result.ServiceAreaPolygons = e.ServiceAreaPolygons;
				result.ServiceAreaPolylines = e.ServiceAreaPolylines;
				tcs.SetResult(result);
				rt.SolveCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				rt.Failed -= fail;
			};
			rt.SolveCompleted += handler;
			rt.Failed += fail;
			rt.SolveAsync(parameters, userToken);
			return tcs.Task;
		}
		public static Task<SolveClosestFacilityResult> SolveClosestFacilityAsyncTask(this RouteTask rt, RouteClosestFacilityParameters parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<SolveClosestFacilityResult>();
			var result = new SolveClosestFacilityResult();
			EventHandler<RouteEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.UserState = e.UserState;
				result.Barriers = e.Barriers;
				result.Facilities = e.Facilities;
				result.Incidents = e.Incidents;
				result.Messages = e.Messages;
				result.PolygonBarriers = e.PolygonBarriers;
				result.PolylineBarriers = e.PolylineBarriers;
				result.RouteResults = e.RouteResults;
				result.ServiceAreaPolygons = e.ServiceAreaPolygons;
				result.ServiceAreaPolylines = e.ServiceAreaPolylines;
				tcs.SetResult(result);
				rt.SolveClosestFacilityCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				rt.Failed -= fail;
			};
			rt.SolveClosestFacilityCompleted += handler;
			rt.Failed += fail;
			rt.SolveClosestFacilityAsync(parameters, userToken);
			return tcs.Task;
		}
		public static Task<SolveServiceAreaResult> SolveServiceAreaAsyncTask(this RouteTask rt, RouteServiceAreaParameters parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<SolveServiceAreaResult>();
			var result = new SolveServiceAreaResult();
			EventHandler<RouteEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.UserState = e.UserState;
				result.Barriers = e.Barriers;
				result.Facilities = e.Facilities;
				result.Incidents = e.Incidents;
				result.Messages = e.Messages;
				result.PolygonBarriers = e.PolygonBarriers;
				result.PolylineBarriers = e.PolylineBarriers;
				result.RouteResults = e.RouteResults;
				result.ServiceAreaPolygons = e.ServiceAreaPolygons;
				result.ServiceAreaPolylines = e.ServiceAreaPolylines;
				tcs.SetResult(result);
				rt.SolveServiceAreaCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				rt.Failed -= fail;
			};
			rt.SolveServiceAreaCompleted += handler;
			rt.Failed += fail;
			rt.SolveServiceAreaAsync(parameters, userToken);
			return tcs.Task;
		}
	}
}
