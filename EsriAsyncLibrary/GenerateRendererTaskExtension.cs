﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class GenerateRendererTaskExtension
	{
		public static Task<GenerateRendererResults> ExecuteAsyncTask(this GenerateRendererTask grt, GenerateRendererParameters generateRendererParameters)
		{
			var tcs = new TaskCompletionSource<GenerateRendererResults>();
			var result = new GenerateRendererResults();
			EventHandler<GenerateRendererResultEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.GenerateRendererResult = e.GenerateRendererResult;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				grt.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				grt.Failed -= fail;
			};
			grt.ExecuteCompleted += handler;
			grt.Failed += fail;
			grt.ExecuteAsync(generateRendererParameters);
			return tcs.Task;
		}
		public static Task<GenerateRendererResults> ExecuteAsyncTask(this GenerateRendererTask grt, GenerateRendererParameters generateRendererParameters, object userToken)
		{
			var tcs = new TaskCompletionSource<GenerateRendererResults>();
			var result = new GenerateRendererResults();
			EventHandler<GenerateRendererResultEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.GenerateRendererResult = e.GenerateRendererResult;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				grt.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				grt.Failed -= fail;
			};
			grt.ExecuteCompleted += handler;
			grt.Failed += fail;
			grt.ExecuteAsync(generateRendererParameters, userToken);
			return tcs.Task;
		}
	}
}
