﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace Test
{
	public static class UploadTaskExtension
	{
		public static Task<DeleteResult> DeleteAsyncTask(this UploadTask ut, string itemID, object userToken = null)
		{
			var tcs = new TaskCompletionSource<DeleteResult>();
			var result = new DeleteResult();
			EventHandler<DeleteEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ut.DeleteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ut.Failed -= fail;
			};
			ut.DeleteCompleted += handler;
			ut.Failed += fail;
			ut.DeleteAsync(itemID, userToken);
			return tcs.Task;
		}
		public static Task<UploadResultExt> UploadAsyncTask(this UploadTask ut, UploadParameters uploadParameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<UploadResultExt>();
			var result = new UploadResultExt();
			EventHandler<UploadEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.UserState = e.UserState;
				result.Result = e.Result;
				tcs.SetResult(result);
				ut.UploadCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ut.Failed -= fail;
			};
			ut.UploadCompleted += handler;
			ut.Failed += fail;
			ut.UploadAsync(uploadParameters, userToken);
			return tcs.Task;
		}
		public static Task<UploadResultExt> UploadAsyncTask(this UploadTask ut, UploadParameters uploadParameters, EventHandler<UploadProgressEventArgs> progressChanged, object userToken = null)
		{
			var tcs = new TaskCompletionSource<UploadResultExt>();
			var result = new UploadResultExt();
			EventHandler<UploadEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.UserState = e.UserState;
				result.Result = e.Result;
				tcs.SetResult(result);
				ut.UploadCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ut.Failed -= fail;
			};
			ut.UploadCompleted += handler;
			ut.UploadProgress += progressChanged;
			ut.Failed += fail;
			ut.UploadAsync(uploadParameters, userToken);
			return tcs.Task;
		}
	}
}
