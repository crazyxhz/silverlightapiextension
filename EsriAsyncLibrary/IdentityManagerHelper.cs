﻿using System;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class GenerateCredentialResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public IdentityManager.Credential Credential { get; set; }
	}
	public class GetCredentialResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public IdentityManager.Credential Credential { get; set; }
	}
}
