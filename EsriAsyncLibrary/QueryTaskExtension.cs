﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class QueryTaskExtension
	{
		/// <summary>
		/// Return QueryResult contain both QueryEventArgs and TaskFailedEventArgs, use TaskSucceed to determine whether Query succeed
		/// </summary>
		/// <param name="qt"></param>
		/// <param name="query">Specifies the attributes and spatial filter of the query.</param>
		/// <returns></returns>
		public static Task<QueryResult> ExecuteAsyncTask(this QueryTask qt, Query query)
		{
			var tcs = new TaskCompletionSource<QueryResult>();
			EventHandler<QueryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> failHandler = null;
			var result = new QueryResult();
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.ExceededTransferLimit = e.ExceededTransferLimit;
				result.FeatureSet = e.FeatureSet;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				qt.ExecuteCompleted -= handler;
			};
			failHandler += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				qt.Failed -= failHandler;
			};
			qt.ExecuteCompleted += handler;
			qt.Failed += failHandler;
			qt.ExecuteAsync(query);
			return tcs.Task;
		}
		/// <summary>
		/// Return QueryResult, use TaskSucceed to determine whether Query succeed
		/// </summary>
		/// <param name="qt"></param>
		/// <param name="query">Specifies the attributes and spatial filter of the query.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <returns></returns>
		public static Task<QueryResult> ExecuteAsyncTask(this QueryTask qt, Query query, object userToken)
		{
			var tcs = new TaskCompletionSource<QueryResult>();
			EventHandler<QueryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> failHandler = null;
			var result = new QueryResult();
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.ExceededTransferLimit = e.ExceededTransferLimit;
				result.FeatureSet = e.FeatureSet;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				qt.ExecuteCompleted -= handler;
			};
			failHandler += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				qt.Failed -= failHandler;
			};
			qt.ExecuteCompleted += handler;
			qt.Failed += failHandler;
			qt.ExecuteAsync(query, userToken);
			return tcs.Task;
		}
		/// <summary>
		/// Executes a query against an ArcGIS Server map layer. The result is returned as a FeatureSet. If the query is successful, return QueryCountEventArgs, else TaskFailedEventArgs
		/// </summary>
		/// <param name="qt"></param>
		/// <param name="query">Specifies the attributes and spatial filter of the query.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <returns></returns>
		//public static Task<EventArgs> ExecuteCountAsyncTask(this QueryTask qt, Query query, object userToken)
		//{
		//	var tcs = new TaskCompletionSource<EventArgs>();
		//	EventHandler<QueryCountEventArgs> handler = null;
		//	EventHandler<TaskFailedEventArgs> failHandler = null;
		//	handler += (s, e) =>
		//	{
		//		tcs.TrySetResult(e);
		//		//Debug.WriteLine(++i);
		//		qt.ExecuteCountCompleted -= handler;
		//	};
		//	failHandler += (s, e) =>
		//	{
		//		tcs.TrySetResult(e);
		//		qt.Failed -= failHandler;
		//	};
		//	qt.ExecuteCountCompleted += handler;
		//	qt.Failed += failHandler;
		//	qt.ExecuteCountAsync(query, userToken);
		//	return tcs.Task;
		//}
		/// <summary>
		/// Executes a query to a obtain addtional data from another layer/table based on a relationship defined in the service meta data. If the query is successful, return RelationshipEventArgs, else TaskFailedEventArgs
		/// </summary>
		/// <param name="qt"></param>
		/// <param name="parameter">The parameters that define the relationship of the query.</param>
		/// <param name="userToken">The user token.</param>
		/// <returns></returns>
		//public static Task<EventArgs> ExecuteRelationshipQueryAsyncTask(this QueryTask qt, RelationshipParameter parameter, object userToken = null)
		//{
		//	var tcs = new TaskCompletionSource<EventArgs>();
		//	EventHandler<RelationshipEventArgs> handler = null;
		//	EventHandler<TaskFailedEventArgs> failHandler = null;
		//	handler += (s, e) =>
		//	{
		//		tcs.TrySetResult(e);
		//		qt.ExecuteRelationshipQueryCompleted -= handler;
		//	};
		//	failHandler += (s, e) =>
		//	{
		//		tcs.TrySetResult(e);
		//		qt.Failed -= failHandler;
		//	};
		//	qt.ExecuteRelationshipQueryCompleted += handler;
		//	qt.Failed += failHandler;
		//	qt.ExecuteRelationshipQueryAsync(parameter, userToken);
		//	return tcs.Task;
		//}
		public static Task<QueryCountResult> ExecuteCountAsyncTask(this QueryTask qt, Query query, object userToken)
		{
			var tcs = new TaskCompletionSource<QueryCountResult>();
			var result = new QueryCountResult();
			EventHandler<QueryCountEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> failHandler = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Count = e.Count;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				//Debug.WriteLine(++i);
				qt.ExecuteCountCompleted -= handler;
			};
			failHandler += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				qt.Failed -= failHandler;
			};
			qt.ExecuteCountCompleted += handler;
			qt.Failed += failHandler;
			qt.ExecuteCountAsync(query, userToken);
			return tcs.Task;
		}

		public static Task<RelationshipQueryResult> ExecuteRelationshipQueryAsyncTask(this QueryTask qt, RelationshipParameter parameter, object userToken = null)
		{
			var tcs = new TaskCompletionSource<RelationshipQueryResult>();
			var result = new RelationshipQueryResult();
			EventHandler<RelationshipEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> failHandler = null;
			handler += (s, e) =>
			{
				result.Result = e.Result;
				result.Succeed = true;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				qt.ExecuteRelationshipQueryCompleted -= handler;
			};
			failHandler += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.TrySetResult(result);
				qt.Failed -= failHandler;
			};
			qt.ExecuteRelationshipQueryCompleted += handler;
			qt.Failed += failHandler;
			qt.ExecuteRelationshipQueryAsync(parameter, userToken);
			return tcs.Task;
		}
	}
}
