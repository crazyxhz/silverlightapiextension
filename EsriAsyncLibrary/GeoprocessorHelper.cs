﻿using ESRI.ArcGIS.Client.Tasks;
using System;

namespace ESRI.ArcGIS.Client.AsyncExtension
{

	public class ExecuteResult
	{
		public bool Succeed { get; set; }
		public GPExecuteResults Results { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
	}

	public class GetInputResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public GPParameter Parameter { get; set; }
	}

	public class GetResultDataResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public GPParameter Parameter { get; set; }
	}

	public class GetResultImageResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public MapImage MapImage { get; set; }
	}
	public class GetResultImageLayerResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public GPResultImageLayer GPResultImageLayer { get; set; }
	}
	public class GetServiceInfoResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public GPServiceInfo GPServiceInfo { get; set; }
	}
	public class SubmitJobResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public JobInfo JobInfo { get; set; }
	}
}
