﻿using ESRI.ArcGIS.Client.Tasks;
using System;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class GenerateRendererResults
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets or sets the renderer result.
		public GenerateRendererResult GenerateRendererResult { get; set; }
	}
}
