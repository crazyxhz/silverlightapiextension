﻿using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class IdentityManagerExtension
	{
		public static Task<GenerateCredentialResult> GenerateCredentialAsyncTask(this IdentityManager im, string url, string userName, string password, IdentityManager.GenerateTokenOptions generateTokenOptions = null)
		{
			var tcs = new TaskCompletionSource<GenerateCredentialResult>();
			var result = new GenerateCredentialResult();
			im.GenerateCredentialAsync(url, userName, password,
				(cre, err) =>
				{
					if (err != null)
					{
						result.Succeed = false;
						result.Error = err;
					}
					else
					{
						result.Succeed = true;
						result.Credential = cre;
					}
					tcs.SetResult(result);
				},
			generateTokenOptions);
			return tcs.Task;
		}
		public static Task<GetCredentialResult> GenerateCredentialAsyncTask(this IdentityManager im, string url, bool retry, IdentityManager.GenerateTokenOptions generateTokenOptions = null)
		{
			var tcs = new TaskCompletionSource<GetCredentialResult>();
			var result = new GetCredentialResult();
			im.GetCredentialAsync(url,retry,
				(cre, err) =>
				{
					if (err != null)
					{
						result.Succeed = false;
						result.Error = err;
					}
					else
					{
						result.Succeed = true;
						result.Credential = cre;
					}
					tcs.SetResult(result);
				},
			generateTokenOptions);
			return tcs.Task;
		}
	}
}
