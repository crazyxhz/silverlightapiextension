﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class FindTaskExtension
	{
		public static Task<FindResultExt> ExecuteAsyncTask(this FindTask ft, FindParameters findParameters)
		{
			var tcs = new TaskCompletionSource<FindResultExt>();
			var result = new FindResultExt();
			EventHandler<FindEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.FindResults = e.FindResults;
				result.ExceededTransferLimit = e.ExceededTransferLimit;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ft.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ft.Failed -= fail;
			};
			ft.ExecuteCompleted += handler;
			ft.Failed += fail;
			ft.ExecuteAsync(findParameters);
			return tcs.Task;
		}

		public static Task<FindResultExt> ExecuteAsyncTask(this FindTask ft, FindParameters findParameters, object userToken)
		{
			var tcs = new TaskCompletionSource<FindResultExt>();
			var result = new FindResultExt();
			EventHandler<FindEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.FindResults = e.FindResults;
				result.ExceededTransferLimit = e.ExceededTransferLimit;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ft.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				ft.Failed -= fail;
			};
			ft.ExecuteCompleted += handler;
			ft.Failed += fail;
			ft.ExecuteAsync(findParameters, userToken);
			return tcs.Task;
		}
	}
}
