﻿using ESRI.ArcGIS.Client.Tasks;
using System;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class ImageServiceIdentifyResults
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The features returned by the IdentifyImageServiceTask for a image service
		//     layer.
		public ImageServiceIdentifyResult Results { get; set; }
	}
}
