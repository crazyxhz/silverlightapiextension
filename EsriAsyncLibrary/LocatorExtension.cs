﻿using ESRI.ArcGIS.Client.Geometry;
using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class LocatorExtension
	{
		public static Task<AddressesToLocationsResults> AddressesToLocationsAsyncTask(this Locator lc, IList<IDictionary<string, string>> Addresses, SpatialReference OutSpatialReference, object UserToken = null)
		{
			var tcs = new TaskCompletionSource<AddressesToLocationsResults>();
			var result = new AddressesToLocationsResults();
			EventHandler<AddressesToLocationsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.AddressesToLocationsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.Failed -= fail;
			};
			lc.AddressesToLocationsCompleted += handler;
			lc.Failed += fail;
			lc.AddressesToLocationsAsync(Addresses, OutSpatialReference, UserToken);
			return tcs.Task;
		}
		public static Task<AddressToLocationsResult> AddressToLocationsAsyncTask(this Locator lc, AddressToLocationsParameters parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<AddressToLocationsResult>();
			var result = new AddressToLocationsResult();
			EventHandler<AddressToLocationsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.AddressToLocationsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.Failed -= fail;
			};
			lc.AddressToLocationsCompleted += handler;
			lc.Failed += fail;
			lc.AddressToLocationsAsync(parameters, userToken);
			return tcs.Task;
		}
		public static Task<LocatorFindResults> FindAsyncTask(this Locator lc, LocatorFindParameters parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<LocatorFindResults>();
			var result = new LocatorFindResults();
			EventHandler<LocatorFindEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.FindCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.Failed -= fail;
			};
			lc.FindCompleted += handler;
			lc.Failed += fail;
			lc.FindAsync(parameters, userToken);
			return tcs.Task;
		}
		public static Task<LocationToAddressResult> LocationToAddressAsyncTask(this Locator lc, MapPoint location, double distance)
		{
			var tcs = new TaskCompletionSource<LocationToAddressResult>();
			var result = new LocationToAddressResult();
			EventHandler<AddressEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Address = e.Address;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.LocationToAddressCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.Failed -= fail;
			};
			lc.LocationToAddressCompleted += handler;
			lc.Failed += fail;
			lc.LocationToAddressAsync(location, distance);
			return tcs.Task;
		}
		public static Task<LocationToAddressResult> LocationToAddressAsyncTask(this Locator lc, MapPoint location, double distance, object userToken)
		{
			var tcs = new TaskCompletionSource<LocationToAddressResult>();
			var result = new LocationToAddressResult();
			EventHandler<AddressEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Address = e.Address;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.LocationToAddressCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				lc.Failed -= fail;
			};
			lc.LocationToAddressCompleted += handler;
			lc.Failed += fail;
			lc.LocationToAddressAsync(location, distance, userToken);
			return tcs.Task;
		}
	}
}
