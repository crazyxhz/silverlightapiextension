﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Collections.Generic;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class AreasAndLengthsResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The AreasAndLengths returned by the geometry service.
		public AreasAndLengths Results { get; set; }
	}
	public class AutoCompleteResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class BufferResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class ConvexHullResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Geometry returned by the service.
		public Geometry.Geometry Result { get; set; }
	}
	public class CutResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The indexes of the new geometries that were derived from the original geometry.
		public int[] CutIndexes;

		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class DensifyResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class DifferenceResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class DistanceResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the distance returned by the geometry service.
		public double Distance { get; set; }
	}
	public class GeneralizeResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class IntersectResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class LabelPointsResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class LengthsResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The lengths returned by the geometry service.
		public List<double> Results { get; set; }
	}
	public class OffsetResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class ProjectResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class RelationResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public List<GeometryRelationPair> Results { get; set; }
	}
	public class ReshapeResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Geometry returned by the service.
		public Geometry.Geometry Result { get; set; }
	}
	public class SimplifyResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class TrimExtendResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Graphics returned by the service.
		public IList<ESRI.ArcGIS.Client.Graphic> Results { get; set; }
	}
	public class UnionResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The Geometry returned by the service.
		public Geometry.Geometry Result { get; set; }
	}
}
