﻿using ESRI.ArcGIS.Client.Tasks;
using System;

namespace Test
{
	public class DeleteResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
	}
	public class UploadResultExt
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the upload result.
		public UploadResult Result { get; set; }
	}
}
