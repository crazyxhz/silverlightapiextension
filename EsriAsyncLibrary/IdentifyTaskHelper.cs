﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Collections.Generic;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class IdentifyResultExt
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets or sets a value indicating whether the number of records returned has
		//     exceeded transfer limit (max record count)
		public bool ExceededTransferLimit { get; set; }
		//
		// Summary:
		//     The features returned by the IdentifyTask.
		public List<IdentifyResult> IdentifyResults { get; set; }
	}
}
