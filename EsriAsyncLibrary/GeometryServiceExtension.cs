﻿using ESRI.ArcGIS.Client.Geometry;
using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class GeometryServiceExtension
	{
		public static Task<AreasAndLengthsResult> AreasAndLengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics)
		{
			var tcs = new TaskCompletionSource<AreasAndLengthsResult>();
			var result = new AreasAndLengthsResult();
			EventHandler<AreasAndLengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.AreasAndLengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.AreasAndLengthsCompleted += handler;
			gs.Failed += fail;
			gs.AreasAndLengthsAsync(graphics);
			return tcs.Task;
		}
		public static Task<AreasAndLengthsResult> AreasAndLengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics, object userToken)
		{
			var tcs = new TaskCompletionSource<AreasAndLengthsResult>();
			var result = new AreasAndLengthsResult();
			EventHandler<AreasAndLengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.AreasAndLengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.AreasAndLengthsCompleted += handler;
			gs.Failed += fail;
			gs.AreasAndLengthsAsync(graphics, userToken);
			return tcs.Task;
		}
		public static Task<AreasAndLengthsResult> AreasAndLengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics, LinearUnit? lengthUnit, LinearUnit? areaUnit, object userToken)
		{
			var tcs = new TaskCompletionSource<AreasAndLengthsResult>();
			var result = new AreasAndLengthsResult();
			EventHandler<AreasAndLengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.AreasAndLengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.AreasAndLengthsCompleted += handler;
			gs.Failed += fail;
			gs.AreasAndLengthsAsync(graphics, lengthUnit, areaUnit, userToken);
			return tcs.Task;
		}
		public static Task<AreasAndLengthsResult> AreasAndLengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics, LinearUnit? lengthUnit, LinearUnit? areaUnit, CalculationType calculationType, object userToken = null)
		{
			var tcs = new TaskCompletionSource<AreasAndLengthsResult>();
			var result = new AreasAndLengthsResult();
			EventHandler<AreasAndLengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.AreasAndLengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.AreasAndLengthsCompleted += handler;
			gs.Failed += fail;
			gs.AreasAndLengthsAsync(graphics, lengthUnit, areaUnit, calculationType, userToken);
			return tcs.Task;
		}
		public static Task<AutoCompleteResult> AutoCompleteAsyncTask(this GeometryService gs, IList<Graphic> polygons, IList<Graphic> polylines)
		{
			var tcs = new TaskCompletionSource<AutoCompleteResult>();
			var result = new AutoCompleteResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.AutoCompleteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.AutoCompleteCompleted += handler;
			gs.Failed += fail;
			gs.AutoCompleteAsync(polygons, polylines);
			return tcs.Task;
		}
		public static Task<AutoCompleteResult> AutoCompleteAsyncTask(this GeometryService gs, IList<Graphic> polygons, IList<Graphic> polylines, object userToken)
		{
			var tcs = new TaskCompletionSource<AutoCompleteResult>();
			var result = new AutoCompleteResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.AutoCompleteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.AutoCompleteCompleted += handler;
			gs.Failed += fail;
			gs.AutoCompleteAsync(polygons, polylines, userToken);
			return tcs.Task;
		}
		public static Task<BufferResult> BufferAsyncTask(this GeometryService gs, BufferParameters bufferParameters)
		{
			var tcs = new TaskCompletionSource<BufferResult>();
			var result = new BufferResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.BufferCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.BufferCompleted += handler;
			gs.Failed += fail;
			gs.BufferAsync(bufferParameters);
			return tcs.Task;
		}
		public static Task<BufferResult> BufferAsyncTask(this GeometryService gs, BufferParameters bufferParameters, object userToken)
		{
			var tcs = new TaskCompletionSource<BufferResult>();
			var result = new BufferResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.BufferCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.BufferCompleted += handler;
			gs.Failed += fail;
			gs.BufferAsync(bufferParameters, userToken);
			return tcs.Task;
		}
		public static Task<ConvexHullResult> ConvexHullAsyncTask(this GeometryService gs, IList<Graphic> graphics)
		{
			var tcs = new TaskCompletionSource<ConvexHullResult>();
			var result = new ConvexHullResult();
			EventHandler<GeometryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.ConvexHullCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.ConvexHullCompleted += handler;
			gs.Failed += fail;
			gs.ConvexHullAsync(graphics);
			return tcs.Task;
		}
		public static Task<ConvexHullResult> ConvexHullAsyncTask(this GeometryService gs, IList<Graphic> graphics, object userToken)
		{
			var tcs = new TaskCompletionSource<ConvexHullResult>();
			var result = new ConvexHullResult();
			EventHandler<GeometryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.ConvexHullCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.ConvexHullCompleted += handler;
			gs.Failed += fail;
			gs.ConvexHullAsync(graphics, userToken);
			return tcs.Task;
		}
		public static Task<CutResult> CutAsyncTask(this GeometryService gs, IList<Graphic> target, ESRI.ArcGIS.Client.Geometry.Polyline cutter)
		{
			var tcs = new TaskCompletionSource<CutResult>();
			var result = new CutResult();
			EventHandler<CutEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.CutIndexes = e.CutIndexes;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.CutCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.CutCompleted += handler;
			gs.Failed += fail;
			gs.CutAsync(target, cutter);
			return tcs.Task;
		}
		public static Task<CutResult> CutAsyncTask(this GeometryService gs, IList<Graphic> target, ESRI.ArcGIS.Client.Geometry.Polyline cutter, object userToken)
		{
			var tcs = new TaskCompletionSource<CutResult>();
			var result = new CutResult();
			EventHandler<CutEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.CutIndexes = e.CutIndexes;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.CutCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.CutCompleted += handler;
			gs.Failed += fail;
			gs.CutAsync(target, cutter, userToken);
			return tcs.Task;
		}
		public static Task<DensifyResult> DensifyAsyncTask(this GeometryService gs, IList<Graphic> graphics, DensifyParameters parameters)
		{
			var tcs = new TaskCompletionSource<DensifyResult>();
			var result = new DensifyResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.DensifyCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.DensifyCompleted += handler;
			gs.Failed += fail;
			gs.DensifyAsync(graphics, parameters);
			return tcs.Task;
		}
		public static Task<DensifyResult> DensifyAsyncTask(this GeometryService gs, IList<Graphic> graphics, DensifyParameters parameters, object userToken)
		{
			var tcs = new TaskCompletionSource<DensifyResult>();
			var result = new DensifyResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.DensifyCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.DensifyCompleted += handler;
			gs.Failed += fail;
			gs.DensifyAsync(graphics, parameters, userToken);
			return tcs.Task;
		}
		public static Task<DifferenceResult> DifferenceAsyncTask(this GeometryService gs, IList<Graphic> geometries, ESRI.ArcGIS.Client.Geometry.Geometry geometry)
		{
			var tcs = new TaskCompletionSource<DifferenceResult>();
			var result = new DifferenceResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.DifferenceCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.DifferenceCompleted += handler;
			gs.Failed += fail;
			gs.DifferenceAsync(geometries, geometry);
			return tcs.Task;
		}
		public static Task<DifferenceResult> DifferenceAsyncTask(this GeometryService gs, IList<Graphic> geometries, ESRI.ArcGIS.Client.Geometry.Geometry geometry, object userToken)
		{
			var tcs = new TaskCompletionSource<DifferenceResult>();
			var result = new DifferenceResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.DifferenceCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.DifferenceCompleted += handler;
			gs.Failed += fail;
			gs.DifferenceAsync(geometries, geometry, userToken);
			return tcs.Task;
		}
		public static Task<DistanceResult> DistanceAsyncTask(this GeometryService gs, ESRI.ArcGIS.Client.Geometry.Geometry geometry1, ESRI.ArcGIS.Client.Geometry.Geometry geometry2, DistanceParameters parameters)
		{
			var tcs = new TaskCompletionSource<DistanceResult>();
			var result = new DistanceResult();
			EventHandler<DistanceEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Distance = e.Distance;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.DistanceCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.DistanceCompleted += handler;
			gs.Failed += fail;
			gs.DistanceAsync(geometry1, geometry2, parameters);
			return tcs.Task;
		}
		public static Task<DistanceResult> DistanceAsyncTask(this GeometryService gs, ESRI.ArcGIS.Client.Geometry.Geometry geometry1, ESRI.ArcGIS.Client.Geometry.Geometry geometry2, DistanceParameters parameters, object userToken)
		{
			var tcs = new TaskCompletionSource<DistanceResult>();
			var result = new DistanceResult();
			EventHandler<DistanceEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Distance = e.Distance;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.DistanceCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.DistanceCompleted += handler;
			gs.Failed += fail;
			gs.DistanceAsync(geometry1, geometry2, parameters, userToken);
			return tcs.Task;
		}
		public static Task<GeneralizeResult> GeneralizeAsyncTask(this GeometryService gs, IList<Graphic> graphics, GeneralizeParameters parameters)
		{
			var tcs = new TaskCompletionSource<GeneralizeResult>();
			var result = new GeneralizeResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.GeneralizeCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.GeneralizeCompleted += handler;
			gs.Failed += fail;
			gs.GeneralizeAsync(graphics, parameters);
			return tcs.Task;
		}
		public static Task<GeneralizeResult> GeneralizeAsyncTask(this GeometryService gs, IList<Graphic> graphics, GeneralizeParameters parameters, object userToken)
		{
			var tcs = new TaskCompletionSource<GeneralizeResult>();
			var result = new GeneralizeResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.GeneralizeCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.GeneralizeCompleted += handler;
			gs.Failed += fail;
			gs.GeneralizeAsync(graphics, parameters, userToken);
			return tcs.Task;
		}
		public static Task<IntersectResult> IntersectAsyncTask(this GeometryService gs, IList<Graphic> geometries, ESRI.ArcGIS.Client.Geometry.Geometry geometry)
		{
			var tcs = new TaskCompletionSource<IntersectResult>();
			var result = new IntersectResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.IntersectCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.IntersectCompleted += handler;
			gs.Failed += fail;
			gs.IntersectAsync(geometries, geometry);
			return tcs.Task;
		}
		public static Task<IntersectResult> IntersectAsyncTask(this GeometryService gs, IList<Graphic> geometries, ESRI.ArcGIS.Client.Geometry.Geometry geometry, object userToken)
		{
			var tcs = new TaskCompletionSource<IntersectResult>();
			var result = new IntersectResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.IntersectCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.IntersectCompleted += handler;
			gs.Failed += fail;
			gs.IntersectAsync(geometries, geometry, userToken);
			return tcs.Task;
		}
		public static Task<LabelPointsResult> LabelPointsAsyncTask(this GeometryService gs, IList<Graphic> graphics)
		{
			var tcs = new TaskCompletionSource<LabelPointsResult>();
			var result = new LabelPointsResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.LabelPointsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.LabelPointsCompleted += handler;
			gs.Failed += fail;
			gs.LabelPointsAsync(graphics);
			return tcs.Task;
		}
		public static Task<LabelPointsResult> LabelPointsAsyncTask(this GeometryService gs, IList<Graphic> graphics, object userToken)
		{
			var tcs = new TaskCompletionSource<LabelPointsResult>();
			var result = new LabelPointsResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.LabelPointsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.LabelPointsCompleted += handler;
			gs.Failed += fail;
			gs.LabelPointsAsync(graphics, userToken);
			return tcs.Task;
		}
		public static Task<LengthsResult> LengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics)
		{
			var tcs = new TaskCompletionSource<LengthsResult>();
			var result = new LengthsResult();
			EventHandler<LengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.LengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.LengthsCompleted += handler;
			gs.Failed += fail;
			gs.LengthsAsync(graphics);
			return tcs.Task;
		}
		public static Task<LengthsResult> LengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics, object userToken)
		{
			var tcs = new TaskCompletionSource<LengthsResult>();
			var result = new LengthsResult();
			EventHandler<LengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.LengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.LengthsCompleted += handler;
			gs.Failed += fail;
			gs.LengthsAsync(graphics, userToken);
			return tcs.Task;
		}
		public static Task<LengthsResult> LengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics, LinearUnit? lengthUnit, bool geodesic, object userToken)
		{
			var tcs = new TaskCompletionSource<LengthsResult>();
			var result = new LengthsResult();
			EventHandler<LengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.LengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.LengthsCompleted += handler;
			gs.Failed += fail;
			gs.LengthsAsync(graphics, lengthUnit, geodesic, userToken);
			return tcs.Task;
		}
		public static Task<LengthsResult> LengthsAsyncTask(this GeometryService gs, IList<Graphic> graphics, LinearUnit? lengthUnit, CalculationType calculationType, object userToken = null)
		{
			var tcs = new TaskCompletionSource<LengthsResult>();
			var result = new LengthsResult();
			EventHandler<LengthsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.LengthsCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.LengthsCompleted += handler;
			gs.Failed += fail;
			gs.LengthsAsync(graphics, lengthUnit, calculationType, userToken);
			return tcs.Task;
		}
		public static Task<OffsetResult> OffsetAsyncTask(this GeometryService gs, IList<Graphic> graphics, OffsetParameters parameters)
		{
			var tcs = new TaskCompletionSource<OffsetResult>();
			var result = new OffsetResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.OffsetCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.OffsetCompleted += handler;
			gs.Failed += fail;
			gs.OffsetAsync(graphics, parameters);
			return tcs.Task;
		}
		public static Task<OffsetResult> OffsetAsyncTask(this GeometryService gs, IList<Graphic> graphics, OffsetParameters parameters, object userToken)
		{
			var tcs = new TaskCompletionSource<OffsetResult>();
			var result = new OffsetResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.OffsetCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.OffsetCompleted += handler;
			gs.Failed += fail;
			gs.OffsetAsync(graphics, parameters, userToken);
			return tcs.Task;
		}
		public static Task<ProjectResult> ProjectAsyncTask(this GeometryService gs, IEnumerable<Graphic> graphics, SpatialReference outSpatialReference)
		{
			var tcs = new TaskCompletionSource<ProjectResult>();
			var result = new ProjectResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.ProjectCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.ProjectCompleted += handler;
			gs.Failed += fail;
			gs.ProjectAsync(graphics, outSpatialReference);
			return tcs.Task;
		}
		public static Task<ProjectResult> ProjectAsyncTask(this GeometryService gs, IEnumerable<Graphic> graphics, SpatialReference outSpatialReference, object userToken)
		{
			var tcs = new TaskCompletionSource<ProjectResult>();
			var result = new ProjectResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.ProjectCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.ProjectCompleted += handler;
			gs.Failed += fail;
			gs.ProjectAsync(graphics, outSpatialReference, userToken);
			return tcs.Task;
		}
		public static Task<ProjectResult> ProjectAsyncTask(this GeometryService gs, IEnumerable<Graphic> graphics, SpatialReference outSpatialReference, DatumTransform datumTransform, bool transformForward = false, object userToken = null)
		{
			var tcs = new TaskCompletionSource<ProjectResult>();
			var result = new ProjectResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.ProjectCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.ProjectCompleted += handler;
			gs.Failed += fail;
			gs.ProjectAsync(graphics, outSpatialReference, datumTransform, transformForward, userToken);
			return tcs.Task;
		}
		public static Task<RelationResult> RelationAsyncTask(this GeometryService gs, IList<Graphic> graphics1, IList<Graphic> graphics2, GeometryRelation spatialRelationship, string comparisonString)
		{
			var tcs = new TaskCompletionSource<RelationResult>();
			var result = new RelationResult();
			EventHandler<RelationEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.RelationCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.RelationCompleted += handler;
			gs.Failed += fail;
			gs.RelationAsync(graphics1, graphics2, spatialRelationship, comparisonString);
			return tcs.Task;
		}
		public static Task<RelationResult> RelationAsyncTask(this GeometryService gs, IList<Graphic> graphics1, IList<Graphic> graphics2, GeometryRelation spatialRelationship, string comparisonString, object userToken)
		{
			var tcs = new TaskCompletionSource<RelationResult>();
			var result = new RelationResult();
			EventHandler<RelationEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.RelationCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.RelationCompleted += handler;
			gs.Failed += fail;
			gs.RelationAsync(graphics1, graphics2, spatialRelationship, comparisonString, userToken);
			return tcs.Task;
		}
		public static Task<ReshapeResult> ReshapeAsyncTask(this GeometryService gs, ESRI.ArcGIS.Client.Geometry.Geometry target, Polyline reshaper)
		{
			var tcs = new TaskCompletionSource<ReshapeResult>();
			var result = new ReshapeResult();
			EventHandler<GeometryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.ReshapeCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.ReshapeCompleted += handler;
			gs.Failed += fail;
			gs.ReshapeAsync(target, reshaper);
			return tcs.Task;
		}
		public static Task<ReshapeResult> ReshapeAsyncTask(this GeometryService gs, ESRI.ArcGIS.Client.Geometry.Geometry target, Polyline reshaper, object userToken)
		{
			var tcs = new TaskCompletionSource<ReshapeResult>();
			var result = new ReshapeResult();
			EventHandler<GeometryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.ReshapeCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.ReshapeCompleted += handler;
			gs.Failed += fail;
			gs.ReshapeAsync(target, reshaper, userToken);
			return tcs.Task;
		}
		public static Task<SimplifyResult> SimplifyAsyncTask(this GeometryService gs, IList<Graphic> graphics)
		{
			var tcs = new TaskCompletionSource<SimplifyResult>();
			var result = new SimplifyResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.SimplifyCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.SimplifyCompleted += handler;
			gs.Failed += fail;
			gs.SimplifyAsync(graphics);
			return tcs.Task;
		}
		public static Task<SimplifyResult> SimplifyAsyncTask(this GeometryService gs, IList<Graphic> graphics, object userToken)
		{
			var tcs = new TaskCompletionSource<SimplifyResult>();
			var result = new SimplifyResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.SimplifyCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.SimplifyCompleted += handler;
			gs.Failed += fail;
			gs.SimplifyAsync(graphics, userToken);
			return tcs.Task;
		}
		public static Task<TrimExtendResult> TrimExtendAsyncTask(this GeometryService gs, IList<Polyline> polylines, Polyline trimExtendTo, CurveExtension extendHow)
		{
			var tcs = new TaskCompletionSource<TrimExtendResult>();
			var result = new TrimExtendResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.TrimExtendCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.TrimExtendCompleted += handler;
			gs.Failed += fail;
			gs.TrimExtendAsync(polylines, trimExtendTo, extendHow);
			return tcs.Task;
		}
		public static Task<TrimExtendResult> TrimExtendAsyncTask(this GeometryService gs, IList<Polyline> polylines, Polyline trimExtendTo, CurveExtension extendHow, object userToken)
		{
			var tcs = new TaskCompletionSource<TrimExtendResult>();
			var result = new TrimExtendResult();
			EventHandler<GraphicsEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.TrimExtendCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.TrimExtendCompleted += handler;
			gs.Failed += fail;
			gs.TrimExtendAsync(polylines, trimExtendTo, extendHow, userToken);
			return tcs.Task;
		}
		public static Task<UnionResult> UnionAsyncTask(this GeometryService gs, IList<Graphic> geometries)
		{
			var tcs = new TaskCompletionSource<UnionResult>();
			var result = new UnionResult();
			EventHandler<GeometryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.UnionCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.UnionCompleted += handler;
			gs.Failed += fail;
			gs.UnionAsync(geometries);
			return tcs.Task;
		}
		public static Task<UnionResult> UnionAsyncTask(this GeometryService gs, IList<Graphic> geometries, object userToken)
		{
			var tcs = new TaskCompletionSource<UnionResult>();
			var result = new UnionResult();
			EventHandler<GeometryEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.UnionCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				gs.Failed -= fail;
			};
			gs.UnionCompleted += handler;
			gs.Failed += fail;
			gs.UnionAsync(geometries, userToken);
			return tcs.Task;
		}
	}
}
