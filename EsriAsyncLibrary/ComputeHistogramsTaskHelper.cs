﻿using System;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class ComputeHistogramsResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the computed histograms result.
		public HistogramsResult Result { get; set; }

	}
}
