﻿using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class ArcGISDynamicMapServiceLayerExtension
	{
		//public static int i = 0;
		public static Task<GetAllDetailsResult> GetAllDetailsTask(this ArcGISDynamicMapServiceLayer layer)
		{
			var tcs = new TaskCompletionSource<GetAllDetailsResult>();
			var result = new GetAllDetailsResult();
			layer.GetAllDetails
			(
				(info, exce) =>
				{
					if (exce != null)
					{
						result.Succeed = false;
						result.Error = exce;
					}
					else
					{
						result.Succeed = true;
						//Debug.WriteLine(i++);
						result.FeatureLayerInfo = info;
					}
					tcs.SetResult(result);
				}
			);
			return tcs.Task;
		}
		public static Task<GetDetailsResult> GetAllDetailsTask(this ArcGISDynamicMapServiceLayer layer, int id)
		{
			var tcs = new TaskCompletionSource<GetDetailsResult>();
			var result = new GetDetailsResult();
			layer.GetDetails
			(id,
				(info, exce) =>
				{
					if (exce != null)
					{
						result.Succeed = false;
						result.Error = exce;
					}
					else
					{
						result.Succeed = true;
						result.FeatureLayerInfo = info;
					}
					tcs.SetResult(result);
				}
			);
			return tcs.Task;
		}
	}
}
