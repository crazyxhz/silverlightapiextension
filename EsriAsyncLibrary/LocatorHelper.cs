﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Collections.Generic;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class AddressesToLocationsResults
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets the addresses to locations result.
		public AddressesToLocationsResult Result { get; set; }
	}

	public class AddressToLocationsResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The address candidates returned by the service.
		public List<AddressCandidate> Results { get; set; }
	}
	public class LocatorFindResults
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     The result returned by the service.
		public LocatorFindResult Result { get; set; }
	}
	public class LocationToAddressResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Gets or sets the address.
		public Address Address { get; set; }
	}
}
