﻿using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public static class ImageServiceIdentifyTaskExtension
	{
		public static Task<ImageServiceIdentifyResults> ExecuteAsyncTask(this ImageServiceIdentifyTask isit, ImageServiceIdentifyParameters parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<ImageServiceIdentifyResults>();
			var result = new ImageServiceIdentifyResults();
			EventHandler<ImageServiceIdentifyEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Results = e.Results;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				isit.ExecuteCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				isit.Failed -= fail;
			};
			isit.ExecuteCompleted += handler;
			isit.Failed += fail;
			isit.ExecuteAsync(parameters,userToken);
			return tcs.Task;
		}
	}
}
