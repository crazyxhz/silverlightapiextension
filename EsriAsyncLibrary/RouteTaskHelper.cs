﻿using ESRI.ArcGIS.Client.Tasks;
using System;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class SolveResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnBarriers
		//     was set to true (which is not the default). If you send in the barriers as
		//     a featureSet (instead of using DataLayer), you already have the barriers
		//     and might not need to request them back from the server.
		public Graphic[] Barriers { get; set; }
		//
		// Summary:
		//     Facilities are returned only if ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters.ReturnFacilities
		//     or ESRI.ArcGIS.Client.Tasks.RouteServiceAreaParameters.ReturnFacilities was
		//     set to true.
		public Graphic[] Facilities { get; set; }
		//
		// Summary:
		//     Incidents are returned only if ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters.ReturnIncidents
		//     was set to true (which is not the default).
		public Graphic[] Incidents { get; set; }
		//
		// Summary:
		//     The solution messages (if returned in the server response) are an array of
		//     ESRI.ArcGIS.Client.Tasks.GPMessage instances.
		public GPMessage[] Messages { get; set; }
		//
		// Summary:
		//     Polygon barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnPolygonBarriers
		//     was set to true (which is not the default).
		public Graphic[] PolygonBarriers { get; set; }
		//
		// Summary:
		//     Polyline barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnPolylineBarriers
		//     was set to true (which is not the default).
		public Graphic[] PolylineBarriers { get; set; }
		//
		// Summary:
		//     The route results is an array of RouteResult instances.
		public RouteResult[] RouteResults { get; set; }
		//
		// Summary:
		//     Service area polygons are returned from ESRI.ArcGIS.Client.Tasks.RouteTask.SolveClosestFacilityAsync(ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters,System.Object)
		public Graphic[] ServiceAreaPolygons { get; set; }
		//
		// Summary:
		//     Service area polylines are returned from ESRI.ArcGIS.Client.Tasks.RouteTask.SolveClosestFacilityAsync(ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters,System.Object)
		public Graphic[] ServiceAreaPolylines { get; set; }
	}
	public class SolveClosestFacilityResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnBarriers
		//     was set to true (which is not the default). If you send in the barriers as
		//     a featureSet (instead of using DataLayer), you already have the barriers
		//     and might not need to request them back from the server.
		public Graphic[] Barriers { get; set; }
		//
		// Summary:
		//     Facilities are returned only if ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters.ReturnFacilities
		//     or ESRI.ArcGIS.Client.Tasks.RouteServiceAreaParameters.ReturnFacilities was
		//     set to true.
		public Graphic[] Facilities { get; set; }
		//
		// Summary:
		//     Incidents are returned only if ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters.ReturnIncidents
		//     was set to true (which is not the default).
		public Graphic[] Incidents { get; set; }
		//
		// Summary:
		//     The solution messages (if returned in the server response) are an array of
		//     ESRI.ArcGIS.Client.Tasks.GPMessage instances.
		public GPMessage[] Messages { get; set; }
		//
		// Summary:
		//     Polygon barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnPolygonBarriers
		//     was set to true (which is not the default).
		public Graphic[] PolygonBarriers { get; set; }
		//
		// Summary:
		//     Polyline barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnPolylineBarriers
		//     was set to true (which is not the default).
		public Graphic[] PolylineBarriers { get; set; }
		//
		// Summary:
		//     The route results is an array of RouteResult instances.
		public RouteResult[] RouteResults { get; set; }
		//
		// Summary:
		//     Service area polygons are returned from ESRI.ArcGIS.Client.Tasks.RouteTask.SolveClosestFacilityAsync(ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters,System.Object)
		public Graphic[] ServiceAreaPolygons { get; set; }
		//
		// Summary:
		//     Service area polylines are returned from ESRI.ArcGIS.Client.Tasks.RouteTask.SolveClosestFacilityAsync(ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters,System.Object)
		public Graphic[] ServiceAreaPolylines { get; set; }
	}
	public class SolveServiceAreaResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		// Summary:
		//     Barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnBarriers
		//     was set to true (which is not the default). If you send in the barriers as
		//     a featureSet (instead of using DataLayer), you already have the barriers
		//     and might not need to request them back from the server.
		public Graphic[] Barriers { get; set; }
		//
		// Summary:
		//     Facilities are returned only if ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters.ReturnFacilities
		//     or ESRI.ArcGIS.Client.Tasks.RouteServiceAreaParameters.ReturnFacilities was
		//     set to true.
		public Graphic[] Facilities { get; set; }
		//
		// Summary:
		//     Incidents are returned only if ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters.ReturnIncidents
		//     was set to true (which is not the default).
		public Graphic[] Incidents { get; set; }
		//
		// Summary:
		//     The solution messages (if returned in the server response) are an array of
		//     ESRI.ArcGIS.Client.Tasks.GPMessage instances.
		public GPMessage[] Messages { get; set; }
		//
		// Summary:
		//     Polygon barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnPolygonBarriers
		//     was set to true (which is not the default).
		public Graphic[] PolygonBarriers { get; set; }
		//
		// Summary:
		//     Polyline barriers are returned only if ESRI.ArcGIS.Client.Tasks.BaseRouteParameters.ReturnPolylineBarriers
		//     was set to true (which is not the default).
		public Graphic[] PolylineBarriers { get; set; }
		//
		// Summary:
		//     The route results is an array of RouteResult instances.
		public RouteResult[] RouteResults { get; set; }
		//
		// Summary:
		//     Service area polygons are returned from ESRI.ArcGIS.Client.Tasks.RouteTask.SolveClosestFacilityAsync(ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters,System.Object)
		public Graphic[] ServiceAreaPolygons { get; set; }
		//
		// Summary:
		//     Service area polylines are returned from ESRI.ArcGIS.Client.Tasks.RouteTask.SolveClosestFacilityAsync(ESRI.ArcGIS.Client.Tasks.RouteClosestFacilityParameters,System.Object)
		public Graphic[] ServiceAreaPolylines { get; set; }
	}
}
