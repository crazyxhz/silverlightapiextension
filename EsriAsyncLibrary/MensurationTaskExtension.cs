﻿using ESRI.ArcGIS.Client.Geometry;
using ESRI.ArcGIS.Client.Tasks;
using System;
using System.Threading.Tasks;

namespace Test
{
	public static class MensurationTaskExtension
	{
		public static Task<AreaAndPerimeterResult> AreaAndPerimeterAsyncTask(this MensurationTask mt, Envelope surfaceArea, MensurationAreaParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<AreaAndPerimeterResult>();
			var result = new AreaAndPerimeterResult();
			EventHandler<MensurationAreaEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.AreaAndPerimeterCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.AreaAndPerimeterCompleted += handler;
			mt.Failed += fail;
			mt.AreaAndPerimeterAsync(surfaceArea, parameters, userToken);
			return tcs.Task;
		}
		public static Task<AreaAndPerimeterResult> AreaAndPerimeterAsyncTask(this MensurationTask mt, ESRI.ArcGIS.Client.Geometry.Polygon surfaceArea, MensurationAreaParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<AreaAndPerimeterResult>();
			var result = new AreaAndPerimeterResult();
			EventHandler<MensurationAreaEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.AreaAndPerimeterCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.AreaAndPerimeterCompleted += handler;
			mt.Failed += fail;
			mt.AreaAndPerimeterAsync(surfaceArea, parameters, userToken);
			return tcs.Task;
		}
		public static Task<CentroidResult> CentroidAsyncTask(this MensurationTask mt, Envelope surfaceArea, MensurationPointParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<CentroidResult>();
			var result = new CentroidResult();
			EventHandler<MensurationPointEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.CentroidCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.CentroidCompleted += handler;
			mt.Failed += fail;
			mt.CentroidAsync(surfaceArea, parameters, userToken);
			return tcs.Task;
		}
		public static Task<CentroidResult> CentroidAsyncTask(this MensurationTask mt, ESRI.ArcGIS.Client.Geometry.Polygon surfaceArea, MensurationPointParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<CentroidResult>();
			var result = new CentroidResult();
			EventHandler<MensurationPointEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.CentroidCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.CentroidCompleted += handler;
			mt.Failed += fail;
			mt.CentroidAsync(surfaceArea, parameters, userToken);
			return tcs.Task;
		}
		public static Task<DistanceAndAngleResult> DistanceAndAngleAsyncTask(this MensurationTask mt, MapPoint point1, MapPoint point2, MensurationLengthParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<DistanceAndAngleResult>();
			var result = new DistanceAndAngleResult();
			EventHandler<MensurationLengthEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.DistanceAndAngleCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.DistanceAndAngleCompleted += handler;
			mt.Failed += fail;
			mt.DistanceAndAngleAsync(point1, point2, parameters, userToken);
			return tcs.Task;
		}
		public static Task<HeightFromBaseAndTopResult> HeightFromBaseAndTopAsyncTask(this MensurationTask mt, MapPoint basePoint, MapPoint topPoint, MensurationHeightParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<HeightFromBaseAndTopResult>();
			var result = new HeightFromBaseAndTopResult();
			EventHandler<MensurationHeightEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.HeightFromBaseAndTopCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.HeightFromBaseAndTopCompleted += handler;
			mt.Failed += fail;
			mt.HeightFromBaseAndTopAsync(basePoint, topPoint, parameters, userToken);
			return tcs.Task;
		}
		public static Task<HeightFromBaseAndTopShadowResult> HeightFromBaseAndTopShadowAsyncTask(this MensurationTask mt, MapPoint basePoint, MapPoint topShadowPoint, MensurationHeightParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<HeightFromBaseAndTopShadowResult>();
			var result = new HeightFromBaseAndTopShadowResult();
			EventHandler<MensurationHeightEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.HeightFromBaseAndTopShadowCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.HeightFromBaseAndTopShadowCompleted += handler;
			mt.Failed += fail;
			mt.HeightFromBaseAndTopShadowAsync(basePoint, topShadowPoint, parameters, userToken);
			return tcs.Task;
		}
		public static Task<HeightFromTopAndTopShadowResult> HeightFromTopAndTopShadowAsyncTask(this MensurationTask mt, MapPoint topPoint, MapPoint topShadowPoint, MensurationHeightParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<HeightFromTopAndTopShadowResult>();
			var result = new HeightFromTopAndTopShadowResult();
			EventHandler<MensurationHeightEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.HeightFromTopAndTopShadowCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.HeightFromTopAndTopShadowCompleted += handler;
			mt.Failed += fail;
			mt.HeightFromTopAndTopShadowAsync(topPoint, topShadowPoint, parameters, userToken);
			return tcs.Task;
		}
		public static Task<PointResult> PointAsyncTask(this MensurationTask mt, MapPoint point, MensurationPointParameter parameters, object userToken = null)
		{
			var tcs = new TaskCompletionSource<PointResult>();
			var result = new PointResult();
			EventHandler<MensurationPointEventArgs> handler = null;
			EventHandler<TaskFailedEventArgs> fail = null;
			handler += (s, e) =>
			{
				result.Succeed = true;
				result.Result = e.Result;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.PointCompleted -= handler;
			};
			fail += (s, e) =>
			{
				result.Succeed = false;
				result.Error = e.Error;
				result.UserState = e.UserState;
				tcs.SetResult(result);
				mt.Failed -= fail;
			};
			mt.PointCompleted += handler;
			mt.Failed += fail;
			mt.PointAsync(point, parameters, userToken);
			return tcs.Task;
		}
	}
}
