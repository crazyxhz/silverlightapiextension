﻿using System;
using System.Collections.Generic;

namespace ESRI.ArcGIS.Client.AsyncExtension
{
	public class GetAllDetailsResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public IDictionary<int, ESRI.ArcGIS.Client.FeatureService.FeatureLayerInfo> FeatureLayerInfo { get; set; }
	}
	public class GetDetailsResult
	{
		public bool Succeed { get; set; }
		public object UserState { get; set; }
		public Exception Error { get; set; }
		public ESRI.ArcGIS.Client.FeatureService.FeatureLayerInfo FeatureLayerInfo { get; set; }
	}
}
